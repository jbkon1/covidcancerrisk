"""
A simple script to estimate risks associated with age, cancer and chemotherapy in Covid-19.

Parameters:

Cases_by_age: Numbers of cases per age band
Deaths_by_age: Number of deaths per age band
Age bands are: (8 age bands in all) 0-19; 20-29; 30-39; 40-49; 50-59; 60-69; 70-79; 80+ 

ORChemoDeath: Increased risk of death with chemotherapy; expressed as an odds ratio
"""

################## User parameters - Edit these ####################


# ORCancerChemo = 3.0

##################### Do not edit below here #########################

import numpy as np
import matplotlib.pyplot as plt

# create data input
Age_Ranges = ["0-19", "20-29", "30-39", "40-49", "50-59","60-69","70-79", "80+"]
Cases_by_age = [1152, 4158, 8453, 10121, 12547, 11181, 7001, 3995]
Deaths_by_age = [1, 7, 19, 39, 144, 374, 592, 639]
ORChemoDeath = 3.67

CFR_age_list = []
excess_CFR_cancer_chemo_list = []

for case, death in zip(Cases_by_age, Deaths_by_age):
    CFR = death/case
    CFR_age_list.append(CFR)
    cancer_chemo_death = (case * (ORChemoDeath * (death / (case - death)))) / (1 + (ORChemoDeath * (death / (case - death))))
    excess_CFR_cancer_chemo = (cancer_chemo_death - death) / case
    excess_CFR_cancer_chemo_list.append(excess_CFR_cancer_chemo)

# create plot
N = 8
ind = np.arange(N)
p1 = plt.bar(ind, excess_CFR_cancer_chemo_list, width=0.6, bottom=np.array(CFR_age_list))
p2 = plt.bar(ind, CFR_age_list, width=0.6)
p3 = plt.plot([0., 7.0], [0.03, 0.03], "k--", color="pink")
p4 = plt.plot([0., 7.0], [0.05, 0.05], "k--", color="red")
p5 = plt.plot([0., 7.0], [0.10, 0.10], "k--", color="purple")

# Axis
age_ranges = ['<20', '20-29', '30-39', '40-49', '50-59', '60-69', '70-79', '>80']
plt.xticks(ind, age_ranges)
plt.yticks(np.arange(0, 1, 0.1))
plt.ylabel("CFR")
plt.xlabel("Age")

# Legend
plt.legend((p1[0], p2[0], p3[0], p4[0], p5[0]), ('excess CFR due to cancer and chemotherapy', 'CFR due to age', '3%', '5%', '10%'))

# Title
#plt.title("CFR due to Covid in cancer patients with and without chemotherapy")

plt.show()

plt.savefig("CFR_Covid_5.png")

# Print results

for age, CFR, adjCFR in zip(Age_Ranges, CFR_age_list, excess_CFR_cancer_chemo_list):
	print(age, round(CFR, 6), round(adjCFR, 4))

