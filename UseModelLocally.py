# Use the covidCancer model for our local patients receiving chemotherapy
# Local SACT data
# Gender code: 1 (Male), 2 (Female)

################## User parameters - Edit these ####################

path_input = '\\\\clw-vfandp-001\\User03\\lk083\\Personal Profile\\Desktop'
csv_input = 'SACT.csv'

##################### Do not edit below here #########################

# Import libraries
from covidCancer import *
import os


####


# Print all the columns of the dataframe
pd.set_option('display.max_columns', 500)


####


# Import local chemotherapy data into a pandas dataframe
def importData(path, csvfile):
	os.chdir(path)
	df = pd.read_csv(csvfile)
	return df


# Clean the start date of regimen and diagnosis (multiple columns with possible diagnosis)
def cleanData(dataframe):
	
	# Extract only the year from the start date of regimen
	dataframe['Year'] = dataframe['Start_Date_Of_Regimen'].astype(str).str[0:4]
	
	# Combine the different diagnosis columns into only one
	dataframe['Primary_Diagnosis'] = dataframe['Primary_Diagnosis'].fillna(dataframe['SACT Prev ICD'])
	dataframe['Primary_Diagnosis'] = dataframe['Primary_Diagnosis'].fillna(dataframe['SCR_ICD'])

	# Extract only the first three characters for the diagnosis
	dataframe['Primary_Diagnosis_3CHAR'] = dataframe['Primary_Diagnosis'].astype(str).str[0:3]

	return dataframe


# Keep the possible patients we are interested in.
def selectData(dataframe):

	# Retain only the columns that we are interested in
	df = dataframe[['NHS_Number', 'Local_Patient_Identifier', 'Date_Of_Birth', 'Person_Stated_Gender_Code', 'Primary_Diagnosis', 'Primary_Diagnosis_3CHAR', 'Start_Date_Of_Regimen', 'Year']]
	
	# Select only patients treated between 2017 and 2019 with a primary diagnosis of lung (C34), breast (C50), testicular - germ cells (C62) or brain (C71)
	df = df.loc[(((df['Year'] == '2017') | (df['Year'] == '2018') | (df['Year'] == '2019') ) & ((df['Primary_Diagnosis_3CHAR'] == 'C34') | (df['Primary_Diagnosis_3CHAR'] == 'C50') | (df['Primary_Diagnosis_3CHAR'] == 'C62') | (df['Primary_Diagnosis_3CHAR'] == 'C71') ))]

	# Remove duplicates
	df = df.drop_duplicates()

	return df


# Run the functions above to import, clean and select the SACT data
df = selectData(cleanData(importData(path_input, csv_input)))

